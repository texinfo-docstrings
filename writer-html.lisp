;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the texinfo-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; texinfo-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package #:texinfo-docstrings)

(define-document-format :html "html")

;;;; HTML OUTPUT

(defvar *html-indent* 0)

(defmacro with-html ((stream tag attributes) &body forms)
  (sb-int:once-only ((stream stream) (tag tag))
    `(progn
      (format ,stream "~vT<~(~A~)~{ ~(~A~)=~S~}>~%" *html-indent* ,tag ,attributes)
      (let ((*html-indent* (+ 2 *html-indent*)))
        ,@forms)
      (format ,stream "~vT</~(~A~)>~%" *html-indent* ,tag))))

(defmethod format-document-start (stream package (format (eql :html)))
  (write-line "<html>" stream)
  (with-html (stream :head nil)
    (format-html stream :link '(:rel "stylesheet" :type "text/css" :href "style.css") nil)
    (format-html stream :title nil (package-name package)))
  (write-line "<body>" stream)
  (format-html stream :h1 '(:class "package-name") (package-name package)))

(defmethod format-document-end (stream package (format (eql :html)))
  (write-line "</body></html>" stream))

(defun html-escape (string)
  (with-output-to-string (s)
    (loop for char across string
          do (case char
               (#\< (write-string "&lt;" s))
               (#\> (write-string "&gt;" s))
               (#\& (write-string "&amp;" s))
               (t   (write-char char s))))))

(defun format-html (stream tag attributes string)
  (format stream "~vT<~(~A~)~{ ~(~A~)=~S~}~:[>~;/>~]~@[~A~]~@[</~(~A~)>~]~%"
          *html-indent* tag attributes (not string) string (when string tag)))

(defun html-text (string)
  (with-output-to-string (result)
    (let ((last 0))
      (dolist (symbol/index (parse-docstrings.sbcl::locate-symbols string))
        (write-string (html-escape (subseq string last (first symbol/index))) result)
        (let ((symbol-name (apply #'subseq string symbol/index)))
          (format result "<var>~A</var>" (html-escape symbol-name)))
        (setf last (second symbol/index)))
      (write-string (html-escape (subseq string last)) result))))

(defmethod format-doc (stream (doc parse-docstrings:documentation*) format)
  (let ((name (html-escape (princ-to-string (parse-docstrings:get-name doc)))))
    (with-html (stream :div '(:class "item"))
      (with-html (stream :div '(:class "type"))
        (format-html stream :a (list :name name) 
                     (html-escape (format nil "[~A]" (string-downcase (princ-to-string (parse-docstrings:get-kind doc)))))))
      (with-html (stream :div '(:class "signature"))
        (format-html stream :code '(:class "name") name)
        (let ((ll (parse-docstrings:lambda-list doc)))
          (when ll
            (with-html (stream :span '(:class "args"))
              (dolist (elt ll)
                (labels ((markup-ll (elt)
                           (cond ((member elt lambda-list-keywords)
                                  (format-html stream :code '(:class "llkw") (html-escape (princ-to-string elt))))
                                 ((listp elt)
                                  (write-string "(" stream)
                                  (mapcar #'markup-ll elt)
                                  (write-string ")" stream))
                                 (t
                                  (format-html stream :var () (html-escape (string-downcase (princ-to-string elt))))))))
                  (markup-ll elt)))))))
      (with-html (stream :div '(:class "item-body"))
        (let ((content (parse-docstrings:get-content doc)))
          (format-doc stream content format)))))
  (terpri stream))

(defmethod format-doc (stream
		       (lisp parse-docstrings:lisp-block)
		       (format (eql :html)))
  (format-html stream
	       :pre
	       '(:class "lisp")
	       (html-escape (parse-docstrings:get-string lisp))))

(defmethod format-doc (stream
		       (list parse-docstrings:itemization)
		       (format (eql :html)))
  (with-html (stream :ul '(:class "itemization"))
    (dolist (item (parse-docstrings:get-items list))
      (with-html (stream :li '(:class "item"))
        (format-html-item stream item)))))

(defgeneric format-html-item (stream item))

(defmethod format-html-item (stream item)
  (format-doc stream item :html))

(defmethod format-html-item (stream (item parse-docstrings:section))
  (dolist (b (parse-docstrings:get-blocks item))
    (format-doc stream b :html)))

(defmethod format-doc (stream
		       (section parse-docstrings:section)
		       (format (eql :html)))
  (dolist (b (parse-docstrings:get-blocks section))
    (format-doc stream b format)))

(defmethod format-doc (stream
		       (paragraph parse-docstrings:paragraph)
		       (format (eql :html)))
  (format-html stream
	       :p
	       '()
	       (html-text (parse-docstrings:get-string paragraph))))

(defmethod format-doc (stream
		       (tabulation parse-docstrings:tabulation)
		       (format (eql :html)))
  (with-html (stream :dl '(:class "tabulation"))
    (dolist (i (parse-docstrings:get-items tabulation))
      (format-doc stream i format))))

(defmethod format-doc (stream
		       (item parse-docstrings:tabulation-item)
		       (format (eql :html)))
  (format-html stream :dt
	       '(:class "tabulation-title")
	       (html-escape (parse-docstrings:get-title item)))
  (with-html (stream :dd '(:class "tabulation-body"))
    (format-html-item stream (parse-docstrings:get-body item))))
