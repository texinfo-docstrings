;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(asdf:defsystem texinfo-docstrings
  :depends-on (:parse-docstrings #-sbcl :closer-mop)
  :serial t
  :components ((:file "package")
               (:file "writer-common")
               (:file "writer-html")
               (:file "writer-texinfo")))

;; vim: ft=lisp et
