;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the texinfo-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; texinfo-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; TODO
;;;; * Method documentation untested
;;;; * Method sorting, somehow
;;;; * Index for macros & constants?
;;;; * This is getting complicated enough that tests would be good

(in-package #:texinfo-docstrings)

(defvar *document-output*)

;;;; utilities

(defun flatten (list)
  (cond ((null list)
         nil)
        ((consp (car list))
         (nconc (flatten (car list)) (flatten (cdr list))))
        ((null (cdr list))
         (cons (car list) nil))
        (t
         (cons (car list) (flatten (cdr list))))))

(defun flatten-to-string (list)
  (format nil "~{~A~^~%~}" (flatten list)))

(defun ensure-class-precedence-list (class)
  (unless (class-finalized-p class)
    (finalize-inheritance class))
  (class-precedence-list class))


;;;; document formats

(defclass document-format ()
  ((name :initarg :name :reader document-format-name)
   (pathname-type :initarg :pathname-type :reader document-format-pathname-type)))

(defmethod document-format-pathname-type ((format symbol))
  (document-format-pathname-type (find-document-format format)))

(defparameter *document-formats* (make-hash-table))

(defmacro define-document-format (name type)
  `(setf (gethash ,name *document-formats*)
         (make-instance 'document-format
                        :name ,name
                        :pathname-type ,type)))

(defvar *default-document-format* :html)

(defun find-document-format (format)
  (if (typep format 'document-format)
      format
      (or (gethash format *document-formats*)
          (error "Unknown document format: ~S" format))))

(defgeneric format-doc (stream doc format))

(defun document-package-pathname (package &key format)
  (make-pathname :name (string-downcase (package-name package))
                 :type (document-format-pathname-type
                        (or format *default-document-format*))))

(defmacro with-document-file ((stream pathname format package) &body body)
  (sb-int:once-only ((format format) (package package))
    `(with-open-file (,stream ,pathname
                              :direction :output
                              :if-does-not-exist :create
                              :if-exists :supersede)
       (format-document-start ,stream ,package ,format)
       ,@body
       (format-document-end ,stream ,package ,format))))

(defun document-package (package &key output-file (format *default-document-format*))
  "Create a file containing all available documentation for the
exported symbols of `package' in Texinfo format. If `filename' is not
supplied, a file \"<packagename>.texinfo\" is generated.

The definitions can be referenced using Texinfo statements like
@ref{<doc-type>_<packagename>_<symbol-name>.texinfo}. Texinfo
syntax-significant characters are escaped in symbol names, but if a
docstring contains invalid Texinfo markup, you lose."
  (let ((real-package (find-package package)))
    (unless real-package
      (error "Unknown package: ~S" package))
    (let* ((real-format (find-document-format format))
           (real-output-file
            (if output-file
                (pathname output-file)
                (document-package-pathname package :format real-format)))
           (docs (sort (parse-docstrings:collect-documentation real-package)
                       #'parse-docstrings:documentation<)))
      (with-document-file (f real-output-file format real-package)
        (dolist (doc docs)
          (format-doc f doc format)))
      real-output-file)))
