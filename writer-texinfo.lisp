;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the texinfo-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; texinfo-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package #:texinfo-docstrings)

(define-document-format :texinfo "texinfo")

;;; If T, package names are prepended in the documentation.  This
;;; doesn't affect filenames.  For now this value is sort of hardcoded
;;; in GENERATE-INCLUDES.  Fix that.
(defvar *prepend-package-names*)

(defparameter *texinfo-escaped-chars* "@{}"
  "Characters that must be escaped with #\@ for Texinfo.")

(defparameter *undocumented-packages*
  #+sbcl '(sb-pcl sb-int sb-kernel sb-sys sb-c)
  #-sbcl nil)

(defparameter *character-replacements*
  '((#\* . "star") (#\/ . "slash") (#\+ . "plus")
    (#\< . "lt") (#\> . "gt"))
  "Characters and their replacement names that `alphanumize' uses. If
the replacements contain any of the chars they're supposed to replace,
you deserve to lose.")

(defparameter *characters-to-drop* '(#\\ #\` #\')
  "Characters that should be removed by `alphanumize'.")

(defun alphanumize (original)
  "Construct a string without characters like *`' that will f-star-ck
up filename handling. See `*character-replacements*' and
`*characters-to-drop*' for customization."
  (let ((name (remove-if (lambda (x) (member x *characters-to-drop*))
                         (if (listp original)
                             (flatten-to-string original)
                             (string original))))
        (chars-to-replace (mapcar #'car *character-replacements*)))
    (flet ((replacement-delimiter (index)
             (cond ((or (< index 0) (>= index (length name))) "")
                   ((alphanumericp (char name index)) "-")
                   (t ""))))
      (loop for index = (position-if #'(lambda (x) (member x chars-to-replace))
                                     name)
         while index
         do (setf name (concatenate 'string (subseq name 0 index)
                                    (replacement-delimiter (1- index))
                                    (cdr (assoc (aref name index)
                                                *character-replacements*))
                                    (replacement-delimiter (1+ index))
                                    (subseq name (1+ index))))))
    name))

(defun include-pathname (doc)
  (let* ((kind (parse-docstrings:get-kind doc))
         (name (nstring-downcase
                (if (eq 'package kind)
                    (format nil "package-~A"
			    (alphanumize (parse-docstrings:get-name doc)))
                    (format nil "~A-~A-~A"
                            (case (parse-docstrings:get-kind doc)
                              ((function generic-function) "fun")
                              (structure "struct")
                              (variable "var")
                              (otherwise
			       (symbol-name (parse-docstrings:get-kind doc))))
                            (alphanumize
			     (parse-docstrings:get-package-name doc))
                            (alphanumize
			     (parse-docstrings:get-name doc)))))))
    (make-pathname :name name  :type "texinfo")))

;;; Node names for DOCUMENTATION instances

(defun package-name-prefix (doc)
  (format nil "~@[~A:~]"
	  (and *prepend-package-names*
	       (parse-docstrings:get-package-name doc))))

(defgeneric name-using-kind/name (kind name doc))

(defmethod name-using-kind/name (kind (name string) doc)
  (declare (ignore kind doc))
  name)

(defmethod name-using-kind/name (kind (name symbol) doc)
  (declare (ignore kind))
  (format nil "~A~A" (package-name-prefix doc) name))

(defmethod name-using-kind/name (kind (name list) doc)
  (declare (ignore kind))
  (assert (parse-docstrings:setf-name-p name))
  (format nil "(setf ~A~A)" (package-name-prefix doc) (second name)))

(defmethod name-using-kind/name ((kind (eql 'method)) name doc)
  (format nil "~A~{ ~A~} ~A"
          (name-using-kind/name nil (first name) doc)
          (second name)
          (third name)))

(defun node-name (doc)
  "Returns TexInfo node name as a string for a DOCUMENTATION instance."
  (let ((kind (parse-docstrings:get-kind doc)))
    (format nil "~:(~A~) ~(~A~)"
            kind
	    (name-using-kind/name kind (parse-docstrings:get-name doc) doc))))

;;; Definition titles for DOCUMENTATION instances

(defgeneric title-using-kind/name (kind name doc))

(defmethod title-using-kind/name (kind (name string) doc)
  (declare (ignore kind doc))
  name)

(defmethod title-using-kind/name (kind (name symbol) doc)
  (declare (ignore kind))
  (format nil "~A~A" (package-name-prefix doc) name))

(defmethod title-using-kind/name (kind (name list) doc)
  (declare (ignore kind))
  (assert (parse-docstrings:setf-name-p name))
  (format nil "(setf ~A~A)" (package-name-prefix doc) (second name)))

(defmethod title-using-kind/name ((kind (eql 'method)) name doc)
  (format nil "~{~A ~}~A"
          (second name)
          (title-using-kind/name nil (first name) doc)))

(defun title-name (doc)
  "Returns a string to be used as name of the definition."
  (string-downcase (title-using-kind/name (parse-docstrings:get-kind doc)
					  (parse-docstrings:get-name doc)
					  doc)))


;;;; turning text into texinfo

(defun texinfo-escape (string &optional downcasep)
  "Return STRING with characters in *TEXINFO-ESCAPED-CHARS* escaped
with #\@. Optionally downcase the result."
  (let ((result (with-output-to-string (s)
                  (loop for char across string
                        when (find char *texinfo-escaped-chars*)
                        do (write-char #\@ s)
                        do (write-char char s)))))
    (if downcasep (nstring-downcase result) result)))


;;;; texinfo formatting tools

(defun hide-superclass-p (class-name super-name)
  (let ((super-package (symbol-package super-name)))
    (or
     ;; KLUDGE: We assume that we don't want to advertise internal
     ;; classes in CP-lists, unless the symbol we're documenting is
     ;; internal as well.
     (and (member super-package
                  #.'(mapcar #'find-package *undocumented-packages*))
          (not (eq super-package (symbol-package class-name))))
     ;; KLUDGE: We don't generally want to advertise SIMPLE-ERROR or
     ;; SIMPLE-CONDITION in the CPLs of conditions that inherit them
     ;; simply as a matter of convenience. The assumption here is that
     ;; the inheritance is incidental unless the name of the condition
     ;; begins with SIMPLE-.
     (and (member super-name '(simple-error simple-condition))
          (let ((prefix "SIMPLE-"))
            (mismatch prefix (string class-name) :end2 (length prefix)))
          t ; don't return number from MISMATCH
          ))))

(defun hide-slot-p (symbol slot)
  ;; FIXME: There is no pricipal reason to avoid the slot docs fo
  ;; structures and conditions, but their DOCUMENTATION T doesn't
  ;; currently work with them the way we'd like.
  (not (and (typep (find-class symbol nil) 'standard-class)
            (parse-docstrings::docstring slot t))))

(defun texinfo-anchor (doc)
  (format *document-output* "@anchor{~A}~%" (node-name doc)))

;;; KLUDGE: &AUX *PRINT-PRETTY* here means "no linebreaks please"
(defun texinfo-begin (doc &aux *print-pretty*)
  (let ((kind (parse-docstrings:get-kind doc)))
    (format *document-output* "@~A {~:(~A~)} ~(~A~@[ ~{~A~^ ~}~]~)~%"
            (case kind
              ((package constant variable)
               "defvr")
              ((structure class condition type)
               "deftp")
              (t
               "deffn"))
            (map 'string (lambda (char) (if (eql char #\-) #\Space char))
                 (string kind))
            (title-name doc)
            (parse-docstrings:lambda-list doc))))

(defun texinfo-index (doc)
  (let ((title (title-name doc)))
    (case (parse-docstrings:get-kind doc)
      ((structure type class condition)
       (format *document-output* "@tindex ~A~%" title))
      ((variable constant)
       (format *document-output* "@vindex ~A~%" title))
      ((compiler-macro function method-combination macro generic-function)
       (format *document-output* "@findex ~A~%" title)))))

(defun texinfo-inferred-body (doc)
  (when (member (parse-docstrings:get-kind doc) '(class structure condition))
    (let ((name (parse-docstrings:get-name doc)))
      ;; class precedence list
      (format *document-output*
              "Class precedence list: @code{~(~{@lw{~A}~^, ~}~)}~%~%"
              (remove-if (lambda (class)  (hide-superclass-p name class))
                         (mapcar #'class-name (ensure-class-precedence-list
                                               (find-class name)))))
      ;; slots
      (let ((slots (remove-if (lambda (slot) (hide-slot-p name slot))
                              (class-direct-slots (find-class name)))))
        (when slots
          (format *document-output* "Slots:~%@itemize~%")
          (dolist (slot slots)
            (format *document-output*
                    "@item ~(@code{~A}~#[~:; --- ~]~
                      ~:{~2*~@[~2:*~A~P: ~{@code{@w{~A}}~^, ~}~]~:^; ~}~)~%~%"
                    (slot-definition-name slot)
                    (remove
                     nil
                     (mapcar
                      (lambda (name things)
                        (if things
                            (list name (length things) things)))
                      '("initarg" "reader"  "writer")
                      ;; because I couldn't grok that format string
                      (flet ((symbol-names (list)
                               (mapcar (lambda (x)
                                         (if (or *prepend-package-names*
                                                 (keywordp x))
                                             (format nil "~(~S~)" x)
                                             (format nil "~(~A~)" x)))
                                       list)))
                        (mapcar #'symbol-names
                                (list (slot-definition-initargs slot)
                                      (slot-definition-readers slot)
                                      (slot-definition-writers slot)))))))
            ;; FIXME: Would be neater to handler as children
            (write-texinfo-string (parse-docstrings::docstring slot t)))
          (format *document-output* "@end itemize~%~%"))))))

(defun texinfo-body (doc)
  (write-texinfo-string (parse-docstrings:get-string doc)))

(defun texinfo-end (doc)
  (write-line (case (parse-docstrings:get-kind doc)
                ((package variable constant) "@end defvr")
                ((structure type class condition) "@end deftp")
                (t "@end deffn"))
              *document-output*))

(defun write-texinfo (doc)
  "Writes TexInfo for a DOCUMENTATION instance to *DOCUMENT-OUTPUT*."
  (texinfo-anchor doc)
  (texinfo-begin doc)
  (texinfo-index doc)
  (texinfo-inferred-body doc)
  (texinfo-body doc)
  (texinfo-end doc)
  ;; FIXME: Children should be sorted one way or another
  (mapc #'write-texinfo (parse-docstrings:get-children doc)))

(defmacro with-texinfo-file (pathname &body forms)
  `(with-open-file (*document-output* ,pathname
				      :direction :output
				      :if-does-not-exist :create
				      :if-exists :supersede)
     ,@forms))

(defun generate-includes (directory &rest packages)
  "Create files in `directory' containing Texinfo markup of all
docstrings of each exported symbol in `packages'. `directory' is
created if necessary. If you supply a namestring that doesn't end in a
slash, you lose. The generated files are of the form
\"<doc-type>_<packagename>_<symbol-name>.texinfo\" and can be included
via @include statements. Texinfo syntax-significant characters are
escaped in symbol names, but if a docstring contains invalid Texinfo
markup, you lose."
  (handler-bind ((warning #'muffle-warning))
    (let ((directory (merge-pathnames (pathname directory))))
      (ensure-directories-exist directory)
      (let ((*prepend-package-names* (> (length packages) 1)))
        (dolist (package packages)
          (dolist (doc (parse-docstrings:collect-documentation
                        (find-package package)
                        (string-downcase (etypecase package
                                           (symbol (symbol-name package))
                                           (string package)))))
            (with-texinfo-file
                (merge-pathnames (include-pathname doc) directory)
              (write-texinfo doc)))))
      directory)))

;;;; TEXINFO OUTPUT

(defmethod format-doc (stream
		       (lisp parse-docstrings:lisp-block)
		       (format (eql :texinfo)))
  (format stream "@lisp~%~A~%@end lisp"
          (texinfo-escape (parse-docstrings:get-string lisp))))
